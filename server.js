
// require our dependencies
var express        = require('express');
var expressLayouts = require('express-ejs-layouts');
var bodyParser     = require('body-parser');
var app            = express();
var port           = process.env.PORT || 7000;
var host		   = "127.0.0.1"
var mongo 		   = require('mongodb');
var mongoose 	   = require('mongoose');
var session 	   = require('express-session');
var passport 	   = require('passport');
var flash    	   = require('connect-flash');
var morgan         = require('morgan');
var cookieParser   = require('cookie-parser');



mongoose.connect('mongodb://localhost/dvna', {useMongoClient: true});
mongoose.Promise = global.Promise;

// use ejs and express layouts
app.set('view engine', 'ejs');
app.use(expressLayouts);

require('./config/passport')(passport);

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));


// Passport init
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session

// use body parser
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

// route our app
var router = require('./app/api');
var home = require('./app/home');
var search = require('./app/search');
var login = require('./app/login');
var sign_up = require('./app/sign_up');
var api = require('./app/api');
var logout = require('./app/logout');
var facebook = require('./app/facebook');
var post = require('./app/post');
var view_story = require('./app/view_story');
var unlock_story = require('./app/unlock');
var postcom = require('./app/postcom');


app.use('/', home);
app.use('/search', search);
app.use('/login', login);
app.use('/logout', logout);
app.use('/sign_up', sign_up);
app.use('/api', api);
app.use('/auth/facebook', facebook);
app.use('/post', post);
app.use('/story',view_story);
app.use('/unlock',unlock_story);
app.use('/postcom',postcom);

// set static files (css and images, etc) location
app.use(express.static(__dirname + '/public'));

// start the server
app.listen(port,host, function() {
  console.log('app started');
  console.log('running at http://' + host + ':' + port)

});

