## Project Outline:

### How to begin using

Once you have cloned and are ready to develop:
`npm i` #install node modules
`npm run dev` #run development environment on localhost:8000

## How to git

### Steps we want to use

- Create aissues for features you're adding/want, assign it the appropriate person
- For your assigned tasks, create a new branch off master. Each branch most likely represents a new overall feature. This could involve 1 or more assigned tasks
- Make changes, commit to your branch only, push to origin
- When you've completed a feature, make a pull/merge request
- Let people review your code and make the appropriate changes they request
- Once approved, squash and merge your request into master. Close current branch.
- Tag a reference to the pull/merge request on the original issue(s) 

