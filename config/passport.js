var FacebookStrategy = require('passport-facebook').Strategy;
var User            = require('../app/user');
var configAuth = require('./auth');

module.exports = function(passport) {

	passport.serializeUser(function(user, done){
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done){
		User.findById(id, function(err, user){
			done(err, user);
		});
	});

	passport.use(new FacebookStrategy({
	    clientID: configAuth.facebookAuth.clientID,
	    clientSecret: configAuth.facebookAuth.clientSecret,
	    callbackURL: configAuth.facebookAuth.callbackURL,
	    profileFields: ['id','emails', 'first_name', 'last_name', 'displayName','photos' ]
	  },
	  	function(accessToken, refreshToken, profile, done) {
	  		console.log(profile.id);
	    	process.nextTick(function(){
	    		User.findOne({facebook_id : profile.id}, function(err, user){
	    			if(err){
	    				return done(err);
	    			}
	    			if(user){
	    				console.log("Should be there!!!!!");
	    				return done(null, user);
	    			}else {
	    				var newUser = new User();
	    				newUser.facebook_id = profile.id;
	    				newUser.token = accessToken;
	    				newUser.username = profile.displayName;
	    				newUser.email = profile.emails ? profile.emails[0].value: 'No email';
	    				newUser.firstname = profile.name.givenName;
	    				newUser.lastname = profile.name.familyName;
	    				newUser.icon = profile.photos ? profile.photos[0].value : '/img/faces/unknown-user-pic.jpg';
	    				newUser.coins = 5;
	    				newUser.save(function(err){
	    					if(err)
	    						throw err;
	    					return done(null, newUser);
	    				})
	    				console.log(newUser);
	    				console.log(profile);
	    			}
	    		});
	    	});
	    }

	));


};