// require express
var express = require('express');
var path    = require('path');
var Story 	= require('./story');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

// route for our homepage
router.get('/', function(req, res) {
	if(!req.session.user){
		res.render('pages/login',{
				login : req.session.user
			});
		return;
	}
	var locked = 0;
  	
	//have not getten req.session.story set it to title 'aaa'
	
		console.log(req.session.title);
		Story.findOne({title: req.query.title },function(err,data){
			if(err){
				res.send(err);
			}else if(data){
				console.log(data);
				var newstory = new Story();
				newstory.author = data.author;
				newstory.type = data.type;
				newstory.title = data.title;
				newstory.like = data.like;
				newstory.users = data.users;
				newstory.pictures = data.pictures;
				newstory.comment_Id = data.comment_Id;
				newstory.contain = data.contain;
				newstory._id = data._id;
				//set the first part and second part
				/*if(data.contain[0].length >= 2){
					newstory.contain[0] = data.contain[0].substr(0,2);
					newstory.contain[1] = data.contain[0].substr(2,data.contain[0].length-6);
					
				}else{
					newstory.contain[0] = data.contain[0].substr(2,data.contain[0].length-6);
					newstory.contain[1] = '';
					
				}
				if(data.contain[1].length >= 2){
					newstory.contain[2] = data.contain[1].substr(0,2);
					newstory.contain[3] = data.contain[1].substr(2,data.contain[1].length-1);
					
				}else{
					newstory.contain[2] = data.contain[1].substr(2,data.contain[1].length-1);
					newstory.contain[3] = '';
					
				}*/
				req.session.story = newstory;
				console.log(req.session.story);
				//must loggin
				if(data.users.indexOf(req.session.user.email) != -1){
					locked = 1;
				}
				console.log(locked);
				res.render('pages/read_unlocked_story',{
					story : req.session.story,
					login : req.session.user,
					locked : locked
				});
			}else {
				console.log("cannot find aaa please post first");
				res.render('pages/post',{
					login : req.session.user
				});
			}
		});	
	//}
});

// route for our homepage
router.post('/', function(req, res) {
	console.log('unlocked post');
  	res.render('pages/read_unlocked_story',{
		login : req.session.user
	});
});






module.exports = router;