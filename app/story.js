var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var storySchema = new Schema({
  Story_ID: Number,
  Type : String,
  title : String,
  author: String,
  contain:Array,
  like: Number,
  //user paid
  users : Array,
  pictures: Array,
  comment_Id: Array
});

var Story = mongoose.model('Story', storySchema);

module.exports = Story;