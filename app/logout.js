var express = require('express');
var path    = require('path');
// create our router object
var router = express.Router();

// export our router
module.exports = router;

router.get('/', function (req, res) {
   req.session.user = null;
   res.render('pages/home',{
        		login : req.session.user
        	});
});


module.exports = router;