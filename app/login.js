var express = require('express');
var path    = require('path');
var User 	= require('./user');


// create our router object
var router = express.Router();

// export our router
module.exports = router;


router.get('/', function(req, res){
	console.log("Got a GET request for the homepage");
	res.render('pages/login',{
        		login : req.session.user
    		});
});

router.post('/', function(req, res){
	var email = req.body.email;
    var password = req.body.password;
    User.findOne({email: email ,password: password },function(err,data){
        if(err){
            res.send(err);
        }else if(data){
            console.log("User Login Successful");
            console.log(data);
            req.session.user = data;
            console.log(req.session.user);
            res.render('pages/home',{
            	login : req.session.user
            });
        }else {
        	console.log("Wrong Username Password Combination");
        	res.render('pages/login',{
        		login : false
        	});
        }
    });
    //res.render('pages/home');

});




module.exports = router;