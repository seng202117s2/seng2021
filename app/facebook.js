var express = require('express');
var path    = require('path');
var User 	= require('./user');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var configAuth = require('../config/auth');
var passport = require('passport')

require('../config/passport')(passport);


// create our router object
var router = express.Router();

// export our router
module.exports = router;

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
router.get('/',
    passport.authenticate('facebook', {
      display: 'popup',
      scope: [ 'email', 'basic_info', 'user_photos'],
      profileFields: ['id', 'name', 'displayName', 'username', 'photos', 'emails', 'birthday'],
      failureRedirect: '/login'
}));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
router.get('/callback', function (req, res, next) {
    passport.authenticate('facebook', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (user) {
            req.session.user = user;
            res.render('pages/home',{
                login : req.session.user
            });
        }else{
            res.render('pages/home',{
                login : false
            });
        }
    })(req, res, next);
});

module.exports = router;