// require express
var express = require('express');
var path    = require('path');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

// route for our homepage
router.get('/', function(req, res) {
	console.log("Got a GET request for the homepage");
  	res.render('pages/home');
});



module.exports = router;