var express = require('express');
var path    = require('path');
var User 	= require('./user');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

router.get('/', function(req, res){
	console.log("Got a GET request for the homepage");
	res.render('pages/sign_up',{
		login : req.session.user
	});
});

router.post('/', function(req, res){
	console.log("Got a GET request for the homepage");
	console.log(req.body.email + "\n"+req.body.password);
	var newuser = new User();

	newuser.email = req.body.email;
	newuser.username = req.body.username;
	newuser.firstname = req.body.firstname;
	newuser.lastname = req.body.lastname;
	newuser.password = req.body.password;
	newuser.coins = 5;
	newuser.icon = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHoAegMBEQACEQEDEQH/xAAbAAEAAQUBAAAAAAAAAAAAAAAABwECAwUGBP/EADUQAAEDAgMFBgMIAwAAAAAAAAEAAgMEEQUGEiExQVKSE1FhgZHBU3GhBxYjMkJygtEUImL/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAwQFAQIG/8QAKREBAAICAgIBAgYDAQAAAAAAAAECAxEEEjFRIXGBIiMyM0FhE0JDBf/aAAwDAQACEQMRAD8AnFAQEBAQEBAQEBAQEBAQEBAQEBAQEBB5KzEqSisKiUB53MAu4+QXumO1/EI75qU/VLWSZkjabMpJbd8jms91NHFn2rzzK/xC+DMdO4/jwzRDn2Pb6j+lyeNaPE7drzKT5jTcRSsmY2SJwexwuHNNwVBMTE6laiYmNwvXHRAQEBAQEBAQEBBosw4z/hD/AB6d4bKRd79/Zg7vM8FZwYe/4p8KnJ5HT8NfLipq2R7nFhc3UbuN7ud8ytGKxDKm8zLz6r716eFzJHxu1McWnvCTGyJ03mXsZNJUWebQuIErOA/7Hd4qrnw9o+PK5xs80t8+Hdg3CzWuqgICAgICAgICCyaRsUT5HmzWNLifALsRudOTOo3KLcQq31VQ+R5Op7i93zP9CwWxSsVjUMHJeb23Ly6l7RmpA1ILo5NLwTtHEd44o7CTMvVBqcHpnuN3Buhx7y029lkZq9ckw3OPbtiiWxUSYQEBAQEBAQEGuzC4swSuI+A4eospMP7lUOf9q30Re913k+K2GGtugXQLoF0EgZGfrwd45Z3D6NPuszlxrJ9mtwZ3j+7o1WXBAQEBAQEBAQeDHYzLg1cxouTA+w8bKTFOslfqizxvHaP6ROHXWwwoVugXQLoF0EgZBaRgj3Hc+ocR6NHss3lz+Z9mtwY/K3/bplVXBAQEBAQEBAQUc0OaWuFwRYhCUPYjTOoa+elfvieWjxHA+i2aW7ViXz+SvS01ebUvTzs1IbNSGzUhtKuVqY0mA0kbhZzma3fyN/dZOe3bJMtvjU64ohtlEnEBAQEBAQEBAQcTn/CHENxSBt9IDZwBw4O9vRXeJk/0lnc3D/0j7uG1K8zTUgakGzy9hr8WxWGnAPZA65j3MG/13eaizZOlNpsGL/LeIS20BrQAAANgA4LJbqqAgICAgICAgICC17GyMcx7Q5rhYgi4IRyY2ivOGExYRigZTXEEzO0a0/p2kEfJanHyTkr8sblYoxX1XxLRXUysXPBdEuZaweLCMOZG1o7aQB0z+Lnd3yCycuSb223MGGMVNfy26iTiAgICAgICAgIKEgC5OxBpMQzTh1I50cL3Vczd7KfaB83bh6qamC9v6VsnKx0+PMo7zLir8VrjPJpDrBoYx12saL2F+JuSSVoYsfSumXnyzkt2lqLqRCujc0PGu+ncSOA70ISjgmaqSogijxGVtPUWtrcbRyeIdu8is3JgtE7r8w2MPKpaNW+JdEyRj2hzHBzTuINwq/hbid+FyAgICAgICDDV1VPRwmaqmjhjG9z3ABdiJtOoebWrWN2ly+JZ2hjaRh8OsfGnuxnkPzO+is040z+pTyc2sfohx2KZjqa64qKiSoB/QPw4h/EbT5lWqYa08Qo5M97+ZaiWrllGlzrM4NaLAeSl1CDcsOpdFdSBqQZIamWG4jfYHe07QfJJiJdiZh7aLGJaV2qEyQO5qd5Z9NxXi1It5e65LV8Tp0uG53rIyGyyQ1be6VvZSdQu0+ir24tZ8fC1Tm3jz8uow/NOHVjmxyPdSzO3MnGm58Hbiq18F6/Plcx8rHf48S3lwoVlVAQEHI5hzjHS9pBhvZyPYS19Q/bGw9wH6j9Faxcft82Uc/MivxRwGIYzPWT9rJI+eXhLNtI/a3c3yV2uOKxqGdfJa87mdtfJK+V2qRznO7ybr2jW3QLoF0C6BdAugXQUugzRVcsQ0tddh3scLtPkuaNuiy/mmqw97I43kw320srrtP7HHa0+G5QZMFbrOHk3x/RJmF4jT4pRsqaVxLDsIIsWuG8EcCFn3rNJ1LXx5K5K9qvYvL25/PVdJQ5eldE4sdK9sRe02IB328bAjzU3HrFsnyrcq81xTpEE0zpXAmwAFmtG5o7gtSIYrHdAugXQLoF0C6BdAugXQLoF0C6CQvssqpJpsSje67Wsidbx/wBhf0AHkqXLiI1LR/8APmd2j6JBVJpOW+0aCWpy45lO0veyVryxu02FwbDzU/GtEZPlV5lZti+ESdnL8N/SVpdoY/W3pTs5OR3SU7QdbejRJyO6SnaDrb0aJOR3SU7QdbelND+R3SU7QdbejQ/kd0lO0HW3o0P5HdJTtB1t6ND+R3SU7QdbejQ/kd0lO0HW3pXQ/kd0lO0HW3o0Scjukp2g629HZycjukp2g629HZyfDf0lNwdbekg/ZVTzQPxComYWRyiNkZcLaiNV7eoVPl2idRDR4FLRNrTHpIipNF4q3D2VRuXEfJBrJMuNdukd6oMD8sHhIUGM5Yk4SIMZyxNf86Cn3Yn50A5Zn5ggxnLdUgvZlqoI2usgr92J+dA+7E/OgyNyxJba9BezLDr7ZEGdmWgN8pQe6kwiOncDrcbINnZBVAQEBAQEBAQEBAQEBAQEBB//2Q==";

	newuser.save(function(err, savedUser){
		if(err){
			console.log(err);
			res.render('pages/sign_up',{
				login : false
			});		
		}

		res.render('pages/login',{
			login : req.session.user
		});		

	});

});

module.exports = router;