var express = require('express');
var path    = require('path');
var Story  = require('./story');
var User 	= require('./user');
// create our router object
var router = express.Router();

// export our router
module.exports = router;
// route for homepage
router.get('/', function(req, res){
	var unlock = 0;
	console.log("unlock");
	console.log(req.session.story);
	var email = req.session.user.email;
	var title = req.session.story.title;
	/*var userId = 0;
	var storyId = 0;
	//get id of current session to update database
	Story.findOne({title: title},function(err,data){
		if(err){
            res.send(err);
        }else if(data){
            storyId = data._id;
        }else {
        	console.log("plz go story page first");
        }
		
	});
	console.log(storyId);
	User.findOne({email: email},function(err,data){
		if(err){
				res.send(err);
		}else if(data){
				userId = data._id;
		}else{			
			console.log("bug");
		}
		
	});*/
	
	//assume that price is 1
	if(req.session.user.coins > 0){
		req.session.user.coins = req.session.user.coins - 1;
		User.findOne({email: email},function(err,data){
			console.log(data);	
			data.coins = data.coins - 1;
			
			delete data._id;
			User.update({_id:data._id},data,function(err){});
		});
		Story.findOne({title: title},function(err,data){
			data.users.push(req.session.user.email);
			delete data._id;
			Story.update({_id:data._id},data,function(err){});
		});
		unlock = 1;
	}else{
		console.log('not enough coins');	
		
	}
	res.render('pages/read_unlocked_story',{
					story : req.session.story,
					login : req.session.user,
					locked : unlock
	});
});



// export router
module.exports = router;