
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	username: { type: String, required: true},
	facebook_id : String,
	email: { type: String, required: true},
	password: String,
	firstname: String,
	lastname: String,
	coins : Number,
	icon : String,
	token : String
});

var User = mongoose.model('User', userSchema);

module.exports = User;