var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
  Story_ID: String,
  author: String,
  contain:String,
  ep: Number,
  //user paid
});

var Comment = mongoose.model('comment', commentSchema);

module.exports = Comment;