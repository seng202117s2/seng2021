var express = require('express');
var path    = require('path');
var Story 	= require('./story');
var User 	= require('./user');
var Comment = require('./comment');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

router.get('/', function(req, res){
	console.log("Got a GET request for the homepage");
	res.render('pages/comment',{
		login : req.session.user
	});
});

router.post('/', function(req, res){
	var commentId = 0;
	console.log("now on new story function");
	console.log(req.body.contain + ' '+ ' '+req.body.ep);
	if(Comment == null){
		console.log('story have not exits');
		storyId = 1;
	}else{
		commentId = Comment.count;
	}
	var newcomment = new Comment();
	newcomment.author = req.session.user ? req.session.user.username: "Unkonwn";
	newcomment.contain = req.body.contain;
	newcomment.ep = req.body.ep;
	newcomment.Story_ID = req.session.story._id;
	console.log(newcomment);
	newcomment.save(function(err, savecomment){
		if(err){
			console.log(err);
		}
	});
	//updata database
	
	console.log(req.session.story._id);
	Story.findById(req.session.story._id,function(err,data){
			data.comment_Id.push(newcomment);
			delete data._id;
			req.session.story = data;
			console.log(data);
			Story.update({_id:data._id},data,function(err){});
			res.render('pages/read_unlocked_story',{
				story : req.session.story,
				locked: 1,
        		login : req.session.user
			});
	});
	
	
});


module.exports = router;