// require express
var express = require('express');
var path    = require('path');
var fs = require('fs');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

// route for homepage
router.get('/', function(req, res){
	console.log("Got a GET request for the homepage");
	console.log(req.session.user);
	res.render('pages/home',{
		login : req.session.user
	});
});



// export router
module.exports = router;
